% Git - colaboration
% Szymon Rogalski
% 2022-05


# Getting started

Our project: https://gitlab.com/Pirgos/1_simple_html

[result](https://pirgos.gitlab.io/1_simple_html)

# Source code

in this case we have one file:

https://gitlab.com/Pirgos/1_simple_html/-/blob/main/source.htm

# Project pipeline

https://gitlab.com/Pirgos/1_simple_html/-/ci/editor?branch_name=main

# GitLab functionalities

* git support (commits, branches, editor)
* merge request
* settings, ...


# TODO ( maybe in pairs)

* checkout main branch
* create new branch like 'yourname_fetaure'
* modify html file in created branch (just add hello from yourself)
* verify if new html file is ok, and if yes add, commit change to local repository 
* push new branch on remote repository
* create merge request to main branch
* approve merge to each other (this should create conflicts)
* try to resolve conflicts, to all changes in main branch





